const port = 3003;

//parser do corpo das requisições
const bodyParser = require('body-parser');

//servidor web
const express = require('express');


//instanciando
const server = express();

const allowCors = require('./cors')

//uma req que usa url encoded de formulários
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(allowCors);

server.listen(port, () => {
    console.log(`BACKEND is running on port: ${port}.`);
})

module.exports = server;
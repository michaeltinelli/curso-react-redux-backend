//next -> chain of responsability se ele vai pro prox middleware ou n etc
module.exports = function (req, res, next) {
    
    //response
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    //continua o fluxo da aplicação
    next();
}
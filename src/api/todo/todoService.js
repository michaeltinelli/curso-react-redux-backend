const Todo = require('./todo');

Todo.methods(['get', 'post', 'put', 'delete']);

//retornar um doc atualizado --- \/ validar as validações
Todo.updateOptions({new: true, runValidators: true});

module.exports = Todo;